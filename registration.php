<!-- Lietotāju reģistrācijas lapa -->
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="img/favicon.ico" />
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta charset="UTF-8">
	<title>Dukatisti</title>
</head>
<body class="body">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<?php
require('core.php');
require('connect.php');
require('functions.php'); 
require('addons/mail/mail.php');

$val_error = [0 => "",1 => "",2 => "",3 => "",4 => "",5 => "",6 => "",7 => "",8 =>""];
$gender = "";
$check_val = true;

if (!loggedin() && isset($_GET['code'])){

	$code = mysqli_real_escape_string($connect, $_GET['code']);
	$q = mysqli_query($connect, "SELECT * FROM `invite` WHERE `code` = '$code'");
	$rq = mysqli_fetch_array(mysqli_query($connect, "SELECT `used`, `id` FROM `invite` WHERE `code` = '$code'"));
	$code_id = $rq['id'];
	if(mysqli_num_rows($q)==0){
		header("Location:".$root."?error=1");
	}elseif($rq['used'] == 1){
		header("Location:".$root."?error=2");
	}
	


	// vērtību piešķiršana mainīgajiem
	if(isset($_POST['username'])&&isset($_POST['password'])&&isset($_POST['password_again'])&&isset($_POST['email'])){
		$username = mysqli_real_escape_string($connect, $_POST['username']);
		$password = mysqli_real_escape_string($connect, $_POST['password']);
		$password_again = mysqli_real_escape_string($connect, $_POST['password_again']);
		$password_hash = password_hash($password, PASSWORD_DEFAULT);
		$email = mysqli_real_escape_string($connect, $_POST['email']);

		// lietotājvārda pārbaude
		if(!preg_match("/^[a-zA-Z0-9]*$/", $username )){
			$check_val = false;
			$val_error[0] = "<p>Atļauti tikai latīņu burti un skaitļi</p>";
		}else{
			if(strpos($username, " ") !== false){
				$check_val = false;
				$val_error[0] = "<p>Nedrīkst būt atstarpju</p>";
			}
		}
		if((strlen($username))<=15 && (strlen($username)>=4)){
		}else{
			$check_val = false;
			$val_error[0] = "<p>Garumam jābūt no 4 līdz 15 simboliem</p>";
		}

		// paroles pārbaude
		if(!preg_match("/^[a-zA-Z0-9]*$/", $password)){
			$check_val = false;
			$val_error[1] = "<p>Atļauti tikai latīņu burti un skaitļi</p>";
		}else{
			if(strpos($password, " ") !== false){
				$check_val = false;
				$val_error[1] = "<p>Nedrīkst būt atstarpju</p>";
			}
		}
		if((strlen($password))<=10 && (strlen($password)>=5)){
		}else{
			$check_val = false;
			$val_error[1] = "<p>Garumam jābūt no 5 līdz 10 simboliem</p>";
		}
		
		// e-pasta pārbaude
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			$check_val = false;
			$val_error[4] = "<p>Lūdzu ievadiet pareizu e-pasta adresi</p>";
		}

		$q = mysqli_query($connect, "SELECT `email` FROM `invite` WHERE `code` = '$code'");
		$rq = mysqli_fetch_array($q);
		if($email != $rq['email']){
			$check_val = false;
			$val_error[8] = "<p>Dotais e-pasts nesakrīt ar ielūguma e-pastu</p>";
		}

		// paroles pareizības pārbaude
		if($password!=$password_again){
			$check_val == false;
			$val_error[1] = "<p>Paroles nesakrīt</p>";
		}
		
// Tukšo lauku pārbaude
if (!empty($username)&&!empty($password)&&!empty($password_again)&&!empty($email)){
	// Pārbaude vai nav nevienu kļūdainu lauku
	if($check_val==true){

		$query = "SELECT `username` FROM `users` WHERE `username`='$username'";
		$query_run = mysqli_query($connect, $query);

		if(mysqli_num_rows($query_run)==1){
			$val_error[0] = "<p>Lietotājvards jau pastāv</p>";
		}else{
			$query = "INSERT INTO `users` (`username`,`password`,`invite_code`) VALUES ('$username','$password_hash','$code_id')";
			if($query_run = mysqli_query($connect, $query)){
				mysqli_query($connect, "UPDATE `invite` SET `used` = '1' WHERE `code` = '$code'");
				$get_id = mysqli_fetch_array(mysqli_query($connect, "SELECT `id` FROM `users` ORDER BY id DESC LIMIT 1"), MYSQLI_BOTH);
				mysqli_query($connect, "INSERT INTO `user_role` (`users_id`, `roles_id`) VALUES ('$get_id[id]','user')");
				$query = "INSERT INTO `users_info` (`users_id`,`email`) VALUES ('$get_id[id]','$email')"; 
				if($query_run = mysqli_query($connect, $query)){
					if(registration_mail($email, $username)){
						header('Location: '.$root.'reg_success.php');
					}
				}else{
					echo mysqli_error();
				}
			}else{
				echo mysqli_error();
			}
		}
	}
}else{
					$val_error[7] = "<p>Visiem laukiem ir jābūt aizpildītiem</p>";				}
			
		
	}

	?>

<center>
	<img src="img/Logo.png" alt="" style="width:15%;">
</center>

<div class="input-group edit_form">
	<form role="form" action="" method="POST">
		<table>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="username">Lietotājvārds:</label>
						<?php 
							if(!empty($val_error[0])){
							?>
							<div class="alert alert-danger error_edit">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>	
							<span class="sr-only">Error: </span>
							<?php echo $val_error[0]; ?>
							</div>
							<?php
							}
						?>
						<input type="text" name='username' class="form-control" placeholder="Lietotājvārds" value="<?php if(isset($username)) {echo $username;} ?>">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="password">Parole:</label>
						<?php 
							if(!empty($val_error[1])){
							?>
							<div class="alert alert-danger error_edit">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>	
							<span class="sr-only">Error: </span>
							<?php echo $val_error[1]; ?>
							</div>
							<?php
							}
						?>
						<input type="password" name='password' class="form-control" placeholder="Parole">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="password_again">Atkārtot paroli:</label>
						<input type="password" name='password_again' class="form-control" placeholder="Atkārtot paroli">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="email">E-pasts:</label>
						<?php 
							if(!empty($val_error[4])){
							?>
							<div class="alert alert-danger error_edit">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>	
							<span class="sr-only">Error: </span>
							<?php echo $val_error[4]; ?>
							</div>
							<?php
							}
						?>
						<?php 
							if(!empty($val_error[8])){
							?>
							<div class="alert alert-danger error_edit">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>	
							<span class="sr-only">Error: </span>
							<?php echo $val_error[8]; ?>
							</div>
							<?php
							}
						?>
						<input type="text" name='email' class="form-control" placeholder="E-pasts" value="<?php if(isset($email)) {echo $email;} ?>">
					</div>
				</td>
			</tr>
		</table>
		<center>
		<input type='submit' value="Reģistrēties" style="margin-top:15px">
		</center>
	</form>
</div>

	<!--<div class="reg_form">
	<form action="" method="POST">
	<table>
		<tr><td>Lietotājvārds:</td></tr>
		<tr><td><input type='text' name='username' id='username' value="<?php if(isset($username)) {echo $username;} ?>"></td><td class='error'><?php echo $val_error[0]; ?></td></tr>
		<tr><td>Parole:</td></tr>
		<tr><td><input type='password' name='password'></td><td class='error'><?php echo $val_error[1]; ?></td></tr>
		<tr><td>Atkārtot paroli:</td></tr>
		<tr><td><input type='password' name='password_again'></td></tr>
		<tr><td>Vārds:</td></tr>
		<tr><td><input type='text' name='firstname' value="<?php if(isset($firstname)) {echo $firstname;} ?>"></td><td class='error'><?php echo $val_error[2]; ?></td></tr>
		<tr><td>Uzvārds:</td></tr>
		<tr><td><input type='text' name='lastname' value="<?php if(isset($lastname)) {echo $lastname;} ?>"></td><td class='error'><?php echo $val_error[3]; ?></td></tr>
		<tr><td>Epasts:</td></tr>
		<tr><td><input type='text' name='email' value="<?php if(isset($email)) {echo $email;} ?>"></td><td class='error'><?php echo $val_error[4]; ?></td></tr>
		<tr><td>//Dzimums:</td></tr>
		<tr><td><input type='radio' name='gender' value='male' <?php echo ($gender=='male')?'checked':'' ?> >Vīrietis<input type='radio' name='gender' value='female' <?php echo ($gender=='female')?'checked':'' ?> >Sieviete</td><td class='error'><?php echo $val_error[5]; ?></td></tr>
		<tr><td>//Valsts:</td></tr>
		<tr><td>
			<select name='country'>
				<option value='none'>Izvēlēties..</option>
				<option value='latvia'>Latvija</option>
				<option value='lithuania'>Lietuva</option>
				<option value="estonia">Igaunija</option>
				<option value="uk">Lielbritānija</option>
				<option value="usa">USA</option>
				<option value="germany">Vācija</option>
			</select>
		</td><td class='error'><?php echo $val_error[6]; ?></td></tr>
	</table>
	<input type='submit'>
		<tr><td class='error'><?php echo $val_error[7]; ?></td></tr>
	</form>
	</div> -->
	<?php 
}else if (loggedin()) {
	header("Location: ".$root."");
}else if(!isset($_GET['code'])){
	header("Location: ".$root."");
}

?>


	
</body>
</html>