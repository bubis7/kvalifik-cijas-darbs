<!-- Profila labošana un attiecīgo vērtību pārbaude un saglabāšana datubāzē -->
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="img/favicon.ico" />
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dukatisti</title>
</head>
<body class="body">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<?php 
require('connect.php');
require('core.php');
require('functions.php'); 
include "navbar.php";

if(!loggedin()){
	header("Location:".$root."");
}


$val_error = [0 => "",1 => "",2 => "",3 => "",4 => "",5 => "",6 => "",7 => "", 8 =>"", 9 =>""];
$photo_error = [0 => "", 1 => "", 2 => "", 3 => "", 4 => ""];
$pssw_sucsess = "";
$check_val = true;



$query = "SELECT `firstname`, `lastname`, `gender`, `location` FROM `users_info` where `users_id`='".$_SESSION['user_id']."' ";
if ($query_run = mysqli_query($connect, $query)) {
	$query_num_rows = mysqli_num_rows($query_run);

	if ($query_num_rows==1) {
		$firstname = mysqli_result($query_run,0,'firstname');
		$lastname = mysqli_result($query_run,0,'lastname');
		$gender = mysqli_result($query_run,0,'gender');
		$location = mysqli_result($query_run,0,'location');
	}
}

if(isset($_POST['edit_profile'])){

	if(!preg_match("/^[a-zA-Zēūīāšģķļžčņ]*$/", $_POST['firstname'])){	
			$check_val = false;
			$val_error[2] = "Atļauti tikai burti";
		}else{
			if(strpos($firstname, " ") !== false){
				$check_val = false;
				$val_error[2] = "Nevar būt atstarpes";
			}
		}
	if(!preg_match("/^[a-zA-Zēūīāšģķļžčņ]*$/", $_POST['lastname'])){
			$check_val = false;
			$val_error[3] = "Atļauti tikai burti";
		}else{
			if(strpos($lastname, " ") !== false){
				$check_val = false;
				$val_error[3] = "Nevar būt atstarpes";
			}
		}

	
	if($check_val == true){
		$firstname = mysqli_real_escape_string($connect, $_POST['firstname']);
		$lastname = mysqli_real_escape_string($connect, $_POST['lastname']);
		$gender = mysqli_real_escape_string($connect, $_POST['gender']);
		$location = mysqli_real_escape_string($connect, $_POST['location']);

		$query = "UPDATE `users_info` SET `firstname` = '$firstname', `lastname` = '$lastname', `gender` = '$gender', `location` = '$location' WHERE `users_id`='".$_SESSION['user_id']."' ";
		mysqli_query($connect, $query);

	}

	
}

if(isset($_POST['edit_pass'])){
	if(empty($_POST['password'])){
		$check_val = false;
		$val_error[8] = "Lūdzu ievadiet tagadējo paroli";
	}

	if(!preg_match("/^[a-zA-Z0-9]*$/", $_POST['new_password'])){
		$check_val = false;
		$val_error[1] = "Tikai latīņu burti un skaitļi";
	}else{
		if(strpos($_POST['new_password'], " ") !== false){
			$check_val = false;
			$val_error[1] = "Nevar būt atstarpes";
		}
	}

	if(!(strlen($_POST['new_password']))<=10 && !(strlen($_POST['new_password'])>=5)){
		$check_val = false;
		$val_error[1] = "Garumam jābūt no 5 līdz 10 simboliem";
	}

	$query = "SELECT `password` FROM `users` WHERE `id`='".$_SESSION['user_id']."' ";

	if ($query_run = mysqli_query($connect, $query)) {
		$query_num_rows = mysqli_num_rows($query_run);

		if ($query_num_rows==1) {
			$password_real = mysqli_result($query_run,0,'password');
		}
	}

	if(password_verify($_POST['password'], $password_real)){
		if(($_POST['new_password'] == $_POST['new_password_again']) && ($check_val == true)){
			$set_password = password_hash(mysqli_real_escape_string($connect, $_POST['new_password']),PASSWORD_DEFAULT);
			mysqli_query($connect, "UPDATE `users` set `password` = '$set_password' WHERE `id`='".$_SESSION['user_id']."'");
			$pssw_sucsess = "Parole veiksmīgi nomainīta";
		}else{
			$val_error[9] = "Jaunā parole nesakrīt, lūdzu mēģiniet vēlreiz";
		}
	}else{
		$pssw_sucsess = "Parole nav pareiza";
	}
}

if(isset($_POST['avatar'])){
	$q1 = mysqli_query($connect, "SELECT `avatar` FROM `users_info` WHERE `users_id` = '".$_SESSION['user_id']."'");
	if(mysqli_result($q1,0,0) != "no-avatar.png"){
		$old_avatar = ".." . $root . "img/Avatars/" . mysqli_result($q1,0,0);
	}
	require "addons/resize_image_helper.php";
	$target_dir = "img/Avatars/";
	$img = time() . "_" . basename($_FILES['avatar']['name']);
	$target_file = $target_dir . $img;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	$uploadOk = 1;
	$check = @getimagesize($_FILES['avatar']['tmp_name']);
	if($check !== false){
		$uploadOk = 1;
	}else{
		$photo_error[0] = "Fails nav attēls";
		$uploadOk = 0;
	}
	if(file_exists($target_file)){
		$photo_error[1] =  "Attēls jau pastāv, mēģiniet vēlreiz";
		$uploadOk = 0;
	}
	if($_FILES["avatar"]["size"] > 2097152){
		$photo_error[2] = "Attēls ir pārāk liels";
		$uploadOk = 0;
	}
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"){
		$photo_error[3] = "Atļautie formāti ir JPG, JPEG, PNG";
		$uploadOk = 0;
	}
	if($uploadOk == 0){
		$photo_error[4] = "Notikusi kļūda, lūdzu mēģiniet vēlreiz";
	}else{
		if(move_uploaded_file($_FILES['avatar']['tmp_name'], $target_file)){
			$image = new resize_image_helper($target_file);
			$image->resize_both(100,150);
			if(isset($old_avatar)){
				unlink($old_avatar);
			}
			$q1="UPDATE `users_info` set `avatar` = '$img' WHERE `users_id`='".$_SESSION['user_id']."'";
			if(mysqli_query($connect, $q1)){
			}else{
				echo 'fail';
			}
		}else{
			echo "error";
		}
	}
}

$avatarq=mysqli_query($connect,"SELECT `avatar` FROM `users_info` WHERE `users_id`= '".$_SESSION['user_id']."'");
$avatarrq = mysqli_fetch_array($avatarq);

?>
<div class="input-group edit_form" style="margin-bottom:10px;">
	<form action="" method="POST" enctype="multipart/form-data">
		<table>
			<tr>
				<td>
					<div class="form-group">
						<?php
						echo '<img src="img/Avatars/'.$avatarrq['avatar'].'" width="100px">';
						?>
					</div>
				</td>
			</tr>		
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="avatar">Avatars:</label>
						<?php
							if(!empty($photo_error[0])){
							?>
							<div class="alert alert-danger error_edit">
							<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
	  						<span class='sr-only'>Error:</span>
	 						<?php echo $photo_error[0]; ?>
							</div>
							<?php
							} 
						?>	
						<input type="file" name="avatar">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<input type="submit" value="Augšupielādēt bildi" name="avatar">
					</div>
				</td>
			</tr>
		</table>
	</form>
</div>


<div class="input-group edit_form">
	<form role="form" action="edit_profile.php" method="POST">
		<table>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="firstname">Vārds:</label>
						<?php
							if(!empty($val_error[2])){
							?>
							<div class="alert alert-danger error_edit">
							<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
	  						<span class='sr-only'>Error:</span>
	 						<?php echo $val_error[2]; ?>
							</div>
							<?php
							} 
						?>	
						<input type="text" name="firstname" class="form-control" placeholder="Vārds" value="<?php echo $firstname; ?>">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="lastname">Uzvārds:</label>
						<?php
							if(!empty($val_error[3])){
							?>
							<div class="alert alert-danger error_edit">
							<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
	  						<span class='sr-only'>Error:</span>
	 						<?php echo $val_error[3]; ?>
							</div>
							<?php
							} 
						?>
						<input type="text" name="lastname" class="form-control" placeholder="Uzvārds" value="<?php echo $lastname; ?>">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="">Dzimums:</label>
						<select class="form-control" name='gender'>
							<option>Izvēlēties...</option>
							<option value='male' <?php if($gender == 'male'){echo 'selected="selected"';} ?>>Vīrietis</option>
							<option value='female'<?php if($gender == 'female'){echo 'selected="selected"';} ?>>Sieviete</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="">Dzīvesvieta:</label>
						<input type="text" name="location" class="form-control" placeholder="Dzīvesvieta" value="<?php echo $location;?>">
					</div>
				</td>
			</tr>
		</table>
		<div style="margin-top:10px;">
			<center>
				<input type ="submit" value="Labot profilu" name="edit_profile" class="btn">
			</center>
		</div>
	</form>
</div>
<div class="input-group edit_form" style="margin-top:10px;">
	<form role="form" action="edit_profile.php" method="POST">
		<table>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="password">Parole:</label>
						<?php
							if(!empty($pssw_sucsess)){
							?>
							<div class="alert alert-danger error_edit">
							<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
	  						<span class='sr-only'>Error:</span>
	 						<?php echo $pssw_sucsess; ?>
							</div>
							<?php
							}
							if(!empty($val_error[8])){
							?>
							<div class="alert alert-danger error_edit">
							<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
	  						<span class='sr-only'>Error:</span>
	 						<?php echo $val_error[8]; ?>
							</div>
							<?php
							} 
							if(!empty($val_error[9])){
							?>
							<div class="alert alert-danger error_edit">
							<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
	  						<span class='sr-only'>Error:</span>
	 						<?php echo $val_error[9]; ?>
							</div>
							<?php
							} 
						?>
						<input type="password" name="password" class="form-control" placeholder="Parole">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="new_password">Jaunā parole:</label>
						<?php
							if(!empty($val_error[1])){
							?>
							<div class="alert alert-danger error_edit">
							<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
	  						<span class='sr-only'>Error:</span>
	 						<?php echo $val_error[1]; ?>
							</div>
							<?php
							} 
						?>
						<input type="password" name="new_password" class="form-control" placeholder="Jaunā parole">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<label class="edit_label" for="new_password_again">Atkārtot jauno paroli:</label>
						<input type="password" name="new_password_again" class="form-control" placeholder="Atkārtot jauno paroli">
					</div>
				</td>
			</tr>
		</table>
		<div style="margin-top:10px;">
			<center>
				<input type ="submit" value="Mainīt paroli" name="edit_pass" class="btn">
			</center>
		</div>
	</form>
</div>
<?php include 'footer.php'; ?>
</body>
</html>
