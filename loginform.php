<!-- Autorizācijas cods -->
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="img/favicon.ico" />
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dukatisti</title>
</head>
<body class="body">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	

<?php
require("connect.php");
require("functions.php");
require("core.php");
include("navbar.php");
$login_error = "";
if (isset($_POST['username'])&&isset($_POST['password'])&&isset($_POST['login'])) {
	$username = mysqli_real_escape_string($connect, $_POST['username']);
	$password = mysqli_real_escape_string($connect, $_POST['password']);

	if (!empty($username)&&!empty($password)) {
		$query = "SELECT `id`,`password` FROM `users` WHERE `username`='$username'";
		
		if ($query_run = mysqli_query($connect,$query)){
			$query_num_rows = mysqli_num_rows($query_run);

			if ($query_num_rows==0){
				$login_error = "Nepareizs username/password";

			}else if ($query_num_rows==1){
				$res = mysqli_result($query_run, 0, 'password');
				if (password_verify($password,$res)) {			
				$user_id = mysqli_result($query_run, 0, 'id');
				$_SESSION['user_id']=$user_id;
				header('Location: '.$root.'');
				}else{
					$login_error = "Nepareizs username/password";
				}
			}
		}else{
			echo mysqli_error();
		}

	}else{
		$login_error = "Ievadiet lietotājvardu/paroli";
	}
}
?>

<div class="container">
	<center>
		<div class="login_logo">
			<img src="img/Logo.png">
		</div>
		<div class="input-group login_form">
			<form method="POST" class="login_form form-signin">
				<table>
					<tr>
						<td>
							<div class="form-group">
								<?php
									if(!empty($login_error)){
									?>
									<div class="alert alert-danger error_edit">
									<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
	  								<span class='sr-only'>Error:</span>
	 								<?php echo $login_error; ?>
									</div>
									<?php
									} 
								?>	
								<input type="text" name="username" class="form-control" placeholder="Lietotājvārds">
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="form-group">
								<input type="password" name="password" class="form-control" placeholder="Parole">
							</div>
						</td>
					</tr>
				</table>
				<div>
					<center>
						<input type="submit" value="Ieiet" name="login" class="btn">
					</center>
				</div>
			</form>
		</div>
	</center>
</div>
</body>
</html>






