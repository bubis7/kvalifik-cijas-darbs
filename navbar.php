<!-- Lietotāju navigācija -->
<?php
require_once("functions.php");
//include "core.php";
if(isset($hacker) AND $hacker == 1) {}else die();

if (isset($_GET['logout'])) {
	session_destroy();
	header('Location: '.$root.'');
}
if (isset($_POST["edit"])){
	header('Location: '.$root.'/edit_profile.php');
}
if (isset($_POST['admin'])){
	header('Location: '.$root.'/admin/');
}
?>
	<div>
		<p class="userbar_greeting">
		<?php
		if(loggedin()){
		echo 'Jūs esat sistēmā, '.htmlentities(username_by_id($_SESSION['user_id'])).'!';
		}
		?>
		</p>
	</div>
	<?php

?>
<nav class='navbar navbar-default userbar'>
<div class='container-fluid'>
	<ul class="nav navbar-nav">
		<li>
			<a href="<?php echo $root; ?>">Sākums</a>
		</li>
		<li>
			<a href="<?php echo $root;?>news.php">Jaunumi</a>
		</li>
		<li>
			<a href="<?php echo $root; ?>forum/">Forums</a>
		</li>
		<li>
			<a href="<?php echo $root;?>user/">Lietotāji</a>
		</li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
		<?php 
		if(loggedin() && getadmin() == 1){
		?>
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Admin <span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li>
					<a href="<?php echo $root ?>admin/index.php?users">Lietotāji</a>
				</li>
				<li>
					<a href="<?php echo $root ?>admin/index.php?category">Kategorijas</a>
				</li>
				<li>
					<a href="<?php echo $root ?>admin/index.php?invites">Ielūgumi</a>
				</li>
			</ul>
		</li>
		<?php 
		}
		if(loggedin()){ 
		?>
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Iespējas <span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li>
					<a href="<?php echo $root;?>edit_profile.php">Profils</a>
				</li>
				<li>
					<a href="<?php echo $root;?>invite.php">Ielūgt lietotāju</a>
				</li>
			</ul>
		</li>
		<?php
		}
		if(loggedin()){
			?>
			<li>
				<a href="<?php echo $root;?>index.php?logout=true">Iziet</a>
			</li>
			<?php
		}else{
			?>
			<li>
				<a href="<?php echo $root;?>loginform.php">Ieiet</a>
			</li>
			<?php
		}
		?>
	</ul>
<div>
</nav>	