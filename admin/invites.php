<!-- Administratora ielūgumu pārvaldības lapa -->
<?php
require '../connect.php';

if(getadmin() != 1){
	header("Location:".$root."");
}

if(isset($_GET['del'])){
	mysqli_query($connect, "DELETE FROM `invite` WHERE `id` = '".mysqli_real_escape_string($connect, $_GET['del'])."'");
	header("Location:../admin/index.php?invites");
}
?>
<div class="container invite_list">
<table class="table">
<tr>
	<th>E-pasts</th>
	<th>Uzaicināja</th>
	<th>Datums</th>
	<th>Reģistrējies</th>
	<th>Dzēst</th>
</tr>
<?php
$q = mysqli_query($connect, "SELECT * FROM `invite`");
while($rq = mysqli_fetch_array($q)){
	if($rq['used'] == '1'){
		$used = "Jā";
	}elseif($rq['used'] == '0'){
		$used = "Nē";
	}else{
		$used = "Nezināms";
	}
	echo '<tr>
		<td>'.$rq['email'].'</td>
		<td>'.username_by_id($rq['referer_id']).'</td>
		<td>'.date("d.m.Y", $rq['time']).'</td>
		<td>'.$used.'</td>
		<td><a href="?invites&del='.$rq['id'].'" class="btn btn-danger">Dzēst</a></td>
		</tr>';
}


?>
</table>
</div>