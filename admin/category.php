<!-- Administratora kateogriju skats -->
<?php 
require ("../connect.php");
$q1 = mysqli_query($connect,"SELECT * FROM `cat`");


?>
<div class="container" style="margin-bottom:20px">
	<center>
	<a href="<?php echo $root ?>admin/index.php?add_cat" class="btn btn-primary">Pievienot kategoriju</a>
	<a href="<?php echo $root ?>admin/index.php?add_sub_cat" class="btn btn-primary">Pievienot apakškategoriju</a>
	</center>
</div>

<div class="post_view">
<ul class="list-group">
<?php 
while($rq1 = mysqli_fetch_array($q1)){
?>
	<div class="row">
		<div class="col-md-7">
		<li class="list-group-item">
			<p><?php echo $rq1['title']; ?></p>
			<div style="position: absolute; top:10px; right:10px;" >
				<a class="btn btn-warning btn-sm" href="<?php echo $root ?>admin/index.php?edit_cat=<?php echo $rq1['id'] ?>">Labot </a>
				<a class="btn btn-danger btn-sm" href="<?php echo $root ?>admin/index.php?del_cat=<?php echo $rq1['id'] ?>"> Dzēst</a>
			</div>
		</li>
		</div>
	</div> 
	<ul class="list-group">
<?php
	$q2 = mysqli_query($connect,"SELECT * FROM `sub_cat` WHERE  `cat_id` = ".$rq1['id']."");
	while($rq2 = mysqli_fetch_array($q2)){
		?>
		<div class="row">
			<div class="col-md-7">
				<li class="list-group-item" style="margin-left:50px">
					<p><?php echo $rq2['title']; ?></p>
					<div style="position: absolute; top:10px; right:10px;">
						<a class="btn btn-warning btn-sm" href="<?php echo $root ?>admin/index.php?edit_sub_cat=<?php echo $rq2['id'] ?>">Labot </a>
						<a class="btn btn-danger btn-sm" href="<?php echo $root ?>admin/index.php?del_sub_cat=<?php echo $rq2['id'] ?>"> Dzēst</a>
					</div>
				</li>
			</div>
		</div>
		<?php
	}
	?>
	</ul>
	</li>
	<?php
}
?>
</ul>
</div>