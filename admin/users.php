<!-- Administratora lietotāju pārvaldības lapa -->
<?php
require('../connect.php');
if(!loggedin() || !getadmin()){
	header("Location:".$root."");
}

if(isset($_GET['remove_user']) && is_numeric($_GET['remove_user'])) {
	if(getadmin() != 1){
		header("Location:".$root."");
	}else{
		$checkq = mysqli_query($connect, "SELECT * FROM `users` WHERE id='".mysqli_real_escape_string($connect, $_GET['remove_user'])."'");
		if(mysqli_num_rows($checkq) == 1) {
			if(mysqli_query($connect, "DELETE FROM `users` WHERE id='".mysqli_real_escape_string($connect, $_GET['remove_user'])."'")) {
				if(mysqli_query($connect, "DELETE FROM `users_info` WHERE `users_id` = '".mysqli_real_escape_string($connect, $_GET['remove_user'])."'")) {
					mysqli_query($connect, "DELETE FROM `user_role` WHERE `users_id` = '".mysqli_real_escape_string($connect, $_GET['remove_user'])."'");
					header("Location: ".$root."admin/index.php?users");
				}
			}
		}
	}
}

if(isset($_POST['submit'])){
	$firstname = mysqli_real_escape_string($connect, $_POST['firstname']);
	$lastname = mysqli_real_escape_string($connect, $_POST['lastname']);
	$location = mysqli_real_escape_string($connect, $_POST['location']);
	$role = mysqli_real_escape_string($connect, $_POST['role']);
	$id = $_POST['id'];
	
	mysqli_query($connect, "UPDATE `users_info` SET `firstname` = '$firstname', `lastname` = '$lastname', `location` = '$location' WHERE `users_id` = '$id'");
	mysqli_query($connect, "UPDATE `user_role` SET `role` = '$role' WHERE `users_id` = '$id'");

	header("Location:../admin/index.php?users");

}

if(isset($_GET['edit'])){
	$q = mysqli_query($connect, "SELECT * FROM `users_info` WHERE `users_id` = '".$_GET['edit']."'");
	while($rq = mysqli_fetch_array($q)){
		$firstname = $rq['firstname'];
		$lastname = $rq['lastname'];
		$location = $rq['location'];
	}
	$q = mysqli_query($connect, "SELECT * FROM `user_role` WHERE `users_id` = '".$_GET['edit']."'");
	while($rq = mysqli_fetch_array($q)){
		$role = $rq['role'];
	}
	mysqli_error($connect);
	?>
	<div class="container user_list input-group" style="margin-bottom:10px; padding-bottom:10px;">
	<center>
		<form method="POST">
			<table>
				<tr>
					<td>
						<div class="form-group">
							<label class="edit_label">Vārds:</label>
							<input type="text" name='firstname' class="form-control" placeholder="Vārds" value="<?php if(isset($firstname)) {echo $firstname;} ?>">
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="form-group">
							<label class="edit_label">Uzvārds:</label>
							<input type="text" name="lastname" class="form-control" placeholder="Uzvārds" value="<?php if(isset($lastname)) {echo $lastname;} ?>">
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="form-group">
							<label class="edit_label">Dzīvesvieta:</label>
							<input type="text" name="location" class="form-control" placeholder="Dzīvesvieta" value="<?php if(isset($location)) {echo $location;} ?>">
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="form-group">
							<label class="edit_label">Loma:</label>
							<select class="form-control" name='role'>
								<option value='user' <?php if($role == 'user'){echo 'selected="selected"';} ?>>Lietotājs</option>
								<option value='admin'<?php if($role == 'admin'){echo 'selected="selected"';} ?>>Administrators</option>
							</select>
						</div>
					</td>
				</tr>
			</table>
			<div style="margin-top:10px;">
				<input type="hidden" name="id" <?php echo 'value="'.$_GET['edit'].'"';?>>
				<input type ="submit" value="Labot" name="submit" class="btn">
			</div>
		</form>
		</center>
	</div>
	<?php
}
?>


<div class="container user_list" >
<table class="table"><tr><th>Username</th><th>Vārds</th><th>Uzvārds</th><th>Ēpasts</th><th>Loma</th><th>Labot</th><th>Dzēst</th></tr>
<?php
$query = "SELECT `id` FROM `users` ";
if($usernames = mysqli_query($connect, $query)){
while($user = mysqli_fetch_array($usernames, MYSQLI_BOTH)) {
	$q = mysqli_query($connect, "SELECT * FROM `user_role` WHERE `users_id` = '$user[id]'");
	while($rq = mysqli_fetch_array($q)){
		$role = $rq['role'];
	}
	if($role == 'user'){
		$role = "Lietotājs";
	}elseif($role == 'admin'){
		$role = "Administrators";
	}else{
		$role = "Nezināms";
	}
	echo '<tr><td>'.htmlentities(username_by_id($user["id"])).'</td>
			  <td>'.htmlentities(user_info($user["id"],"firstname")).'</td>
			  <td>'.htmlentities(user_info($user["id"],"lastname")).'</td>
			  <td>'.htmlentities(user_info($user["id"],"email")).'</td>
			  <td>'.$role.'</td>
			  <td><a href="?users&edit='.$user['id'].'" class="btn btn-default">Labot</a></td>
			  <td><a onclick="return confirm(\'Vai tiesam gribi dzēst?\')" href="?users&remove_user='.$user["id"].'" class="btn btn-danger">Dzēst</a></td>
		  </tr>';
}
}else{
	echo mysqli_error($connect);
}
?>
</table>
</div>







