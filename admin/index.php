<!-- Administratora include lapa -->
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="../img/favicon.ico" />
	<link href="../css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dukatisti</title>
</head>
<body class="body">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<?php
require "../core.php";
require "../functions.php";
include "../navbar.php";

if(getadmin()){

if(isset($_GET['users'])){
	include ('users.php');
}elseif(isset($_GET['category'])){
	include ('category.php');
}elseif(isset($_GET['add_cat'])){
	include ('add_cat.php');
}elseif(isset($_GET['add_sub_cat'])){
	include ('add_sub_cat.php');
}elseif(isset($_GET['del_cat'])|| isset($_GET['del_sub_cat'])){
	include ('del_cat.php');
}elseif(isset($_GET['edit_cat'])|| isset($_GET['edit_sub_cat'])){
	include ('edit_cat.php');
}elseif(isset($_GET['invites'])){
	include ('invites.php');
}



}else{
	header("Location:".$root."");
}
?>
	
</body>
</html>
