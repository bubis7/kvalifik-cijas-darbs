<!-- Ielūgumu lapa -->
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="img/favicon.ico" />
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dukatisti</title>
</head>
<body class="body">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<?php
require 'core.php';
require 'connect.php';
require 'functions.php';
require('addons/mail/mail.php');
include 'navbar.php';

$val_error = [0 => "",1 => "",2 => ""];
$check_val = true;

if(isset($_POST['invite'])){
	$email = mysqli_real_escape_string($connect, $_POST['email']);
	
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$check_val = false;
		$val_error[0] = "<p>Lūdzu ievadiet pareizu e-pasta adresi</p>";
	}

	$q = mysqli_query($connect, "SELECT * FROM `users_info` WHERE `email` = '$email'");
	if(mysqli_num_rows($q)>=1){
		$check_val = false;
		$val_error[1] = "<p>Lietotājs ar šādu e-pastu jau ir reģistrēts</p>";
	}
	$q = mysqli_query($connect, "SELECT * FROM `invite` WHERE `email` = '$email'");
	if(mysqli_num_rows($q)==1){
		$check_val = false;
		$val_error[2] = "<p>Lietotājs ar šādu e-pastu jau ir ielūgts</p>";
	}
	if($check_val){
		$code = uniqid();
		$username = username_by_id($_SESSION['user_id']);
		$time = time();
		
		if($q1 = mysqli_query($connect, "INSERT INTO `invite` (`email`,`code`,`time`,`referer_id`) VALUES ('$email','$code','$time','".$_SESSION['user_id']."')")){
			if(invite_mail($email,$username,$code)){}
		}
	}
}


if(isset($_GET['sent'])){
	?>
	<div class="invite_sent">
		<center>
		<p>Ielūgums ir nosūtīts</p>
		</center>
	</div>
	<?php
}

?>
<div class="input-group edit_form">
	<form method="POST">
		<table>
			<tr>
				<td>
					<div class="form-group">
						<p>Tāpat kā jūs ielūdza šajā forumā, tā arī jebkurš cits lietotājs drīkst reģistrēties tikai ar ielūgumiem. Ja uzskati, ka ir kāds cilvēks, kas spēs dot kaut ko pozitīvu, droši aicini viņu!</p>
						<p>Apakšā ievadi tā cilvēka, kuru vēlies aicināt, ēpastu un uz to tiks nosūtīts uzaicinājums reģistrēties.</p>
						<label for="email">E-pasts</label>
						<?php 
							if(!empty($val_error[0])){
							?>
							<div class="alert alert-danger error_edit">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>	
							<span class="sr-only">Error: </span>
							<?php echo $val_error[0]; ?>
							</div>
							<?php
							}
							if(!empty($val_error[1])){
							?>
							<div class="alert alert-danger error_edit">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>	
							<span class="sr-only">Error: </span>
							<?php echo $val_error[1]; ?>
							</div>
							<?php
							}
							if(!empty($val_error[2])){
							?>
							<div class="alert alert-danger error_edit">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>	
							<span class="sr-only">Error: </span>
							<?php echo $val_error[2]; ?>
							</div>
							<?php
							}
						?>
						<input type="text" name="email" class="form-control" placeholder="E-pasts">
					</div>
				</td>
			</tr>
		</table>
		<center>
			<input type="submit" value="Ielūgt" name="invite" style="margin-top:15px;">
		</center>
	</form>
</div>	
<?php include 'footer.php';?>
</body>
</html>