<!-- Lietotāju apskates lapa -->
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="../img/favicon.ico" />
	<link href="../css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dukatisti</title>
</head>
<body class="body">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<?php 
	require "../connect.php";
	require "../core.php";
	require "../functions.php";
	include "../navbar.php";
	?>
<div class="user_view">
<?php

	if(isset($_GET['user'])){
		$q1 = mysqli_query($connect, "SELECT * FROM `users` WHERE `id` = '".$_GET['user']."'");
		$q2 = mysqli_query($connect, "SELECT * FROM `users_info` WHERE `users_id` = '".$_GET['user']."'");
		$rq1 = mysqli_fetch_array($q1);
		$rq2 = mysqli_fetch_array($q2);
		if($rq2['gender']=='male'){
			$gender = 'Vīrietis';
		}elseif($rq2['gender']=='female'){
			$gender = 'Sieviete';
		}else{
			$gender = 'Nav norādīts';
		}
		?>
		<div class="row">
			<div class="col-md-2">
				<img src="../img/Avatars/<?php echo $rq2['avatar'];?>" width="100px" alt="">
			</div>
			<div class="col-md-6">
				<b>Lietotājvards: </b><?php echo $rq1['username'];?><br>
				<b>Vārds: </b><?php echo $rq2['firstname'];?><br>
				<b>Uzvārds: </b><?php echo $rq2['lastname'];?><br>
				<b>Dzimums: </b><?php echo $gender;?><br>
				<b>Dzīvesvieta: </b><?php echo $rq2['location'];?><br>
			</div>
		</div>
		<?php
	

	}else{
	$q1 = mysqli_query($connect, "SELECT * FROM `users`");

	?>
	<table class="table">
	<?php
	while($rq1 = mysqli_fetch_array($q1)){
		?>
		<tr>
			<td>
				<a href="?user=<?php echo $rq1['id'];?>"><?php echo $rq1['username'];?></a>
			</td>
		</tr>
		<?php
	}
	?>
	</table>
	<?php
}
?>
</div>
<?php include '../footer.php';?>
</body>
</html>