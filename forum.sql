
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 08, 2015 at 04:11 PM
-- Server version: 10.0.20-MariaDB
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u874317203_forum`
--

-- --------------------------------------------------------

--
-- Table structure for table `cat`
--

CREATE TABLE IF NOT EXISTS `cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `guest` int(1) NOT NULL,
  `user` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cat`
--

INSERT INTO `cat` (`id`, `title`, `text`, `guest`, `user`) VALUES
(1, 'Par Ducatisti', 'Viss par un ap šo forumu', 1, 1),
(5, 'Ducati', '', 0, 1),
(4, 'Honda', '', 0, 1),
(6, 'Testa kategorija', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posts_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `invite`
--

CREATE TABLE IF NOT EXISTS `invite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) NOT NULL,
  `referer_id` int(11) NOT NULL,
  `used` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `invite`
--

INSERT INTO `invite` (`id`, `email`, `code`, `time`, `referer_id`, `used`) VALUES
(3, 'klusais52@gmail.com', '5606a3dd1164b', 1443275741, 54, 1),
(4, 'guntars.ducati@gmail.com', '560aa7e0c1846', 1443538912, 54, 1),
(5, 'normunds.pauders@rvt.lv', '560ec06d1bce0', 1443807341, 54, 0),
(6, 'martakanepite@inbox.lv', '5614c4227f5ae', 1444201506, 54, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `sub_cat_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=138 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `users_id`, `sub_cat_id`, `title`, `text`, `time`) VALUES
(9, 40, 1, 'Pre-alpha 0.3', 'Pievienota komentāru pievienošana un, protams, tie arī pareizi parādās attiecīgi rakstam. Vismaz vajadzētu. Nākamais būs dizains, cerams. :D', 1428518672),
(5, 40, 1, 'Pre-alpha 0.2', 'Pievienota iespēja dzēst savus rakstus, bet derētu pamainīt veidu, kā tas notiek.', 1425317463),
(10, 49, 1, 'Elite: Dangerous', 'BUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!\r\nBUY IT!!!!!!!!!!!!!!!', 1428911141),
(11, 40, 1, 'Pre-alpha 0.4', 'Dizains pievienots kategorijām, nav protams pabeigts, bet tas ir kaut kas. ', 1428911263),
(108, 54, 1, 'Pre-alpha 0.5', 'Izveidots dizains rakstu sarakstam, kā pāris uzlabojumi under the hood....', 1430300277),
(112, 54, 1, 'Alpha 1.1', 'BEIDZOT kaut kāds rakstu dizains. Un administratoriem pamainīts panelis, var pievienot arī citas kategorijas/apakškategorijas.', 1431544617),
(115, 54, 1, 'Alpha 1.1.1', 'Izlabota kļūda, kad neparādās jaunākais raksts. ', 1431589503),
(116, 54, 1, 'Alpha 1.1.2', 'Izlaboju kļūdu kad neparādās komentāra autora vārds un uzvārds.', 1431590582),
(118, 54, 1, 'Alpha 1.2', 'Administratora panelis papildināts ar iespēju dzēst un rediģēt kategorijas.', 1432627639),
(119, 54, 1, 'Alpha 1.2.1', 'Saņēmos un pievienoju iespēju pievienot rakstus arī citās kategorijās. Vismaz kaut ko šodien izdarīju.', 1433225501),
(120, 54, 1, 'Alpha 1.2.2', 'Pievienoti avatari. Pagidām gan viņus paši nevar pievienot. Ir pieejams tikai noklusējuma avatars.', 1433230822),
(121, 54, 1, 'Alpha 1.3', 'Un nu jau ir iespējams mainīt savus avatarus. Kristap, gaidu no tevis tavu koalu. :D', 1433270612),
(122, 54, 1, 'Alpha 1.3.1', 'Bugfixes. Again. As always.', 1433753061),
(129, 54, 1, 'Alpha 1.4', 'Izveidots un arī pamainīts dizains dažās lapās. Ameriku vēl neesmu atklājis.', 1433831183),
(137, 54, 1, 'Alpha 1.7.9', '<ul><li>Izlabotas kļūdas ar dzimuma izvēli.</li></ul><p>Alpha 1.7.8</p><ul><li>Izlabota kļūda kas noteiktos gadījumos uzrāda divas labo&scaron;anas iespējas pie raksta vai komentāra.</li></ul><p>Alpha 1.7.7</p><ul><li>Pievienota iespēja lietotājam labot savus rakstus un komentārus.</li><li>Pievienota sākumlapa un Jaunumu lapa</li></ul><p>Alpha 1.7.6</p><ul><li>Iespējams salabota problēma ar pārak daudz savienojumiem.</li><li>Pārveidoju lomu sistēmu uz atsevi&scaron;ķām tabulām.</li><li>Pievienoju iespēju administratoram iedalīt lomas.</li></ul><p>Alpha 1.7.5</p><ul><li>Izlaboti vairāki vaicājumi, lai tie nākotnē strādātu neatkarīgi no datubāzes izmaiņām.</li><li>Ielūgumi pabeigti (itkā, ja neko neesmu aizmirsis).</li><li>Izlabotas pāris kļūdas citos vaicājumos.</li><li>Uzlabota lietotaju apskate.</li><li>Pievienots footers.</li></ul><p>Alpha 1.7.4</p><ul><li>Sākts darbs pie lietotāju uzaicinā&scaron;anas.</li><li>Reģistrācija tikai ar ielūgumiem.</li><li>Funkcionālā puse ielūgumiem pabeigta.</li><li>Palicis smuki atrādīt kļūdu paziņojumus utt..</li></ul><p>Alpha 1.7.3</p><ul><li>Izlabotas pāris kļūdas sakarā ar epastu.</li><li>Pievienota iespeja administratoram labot rakstus un comentārus.</li></ul><p>Alpha 1.7.2</p><ul><li>Nereģistrēts lietotājs vairs nevar apskatīt categorijas un rakstus, kas viņam ir liegti ja ir zināma to adrese.</li><li>Nereģistrējies lietotājs var apskatīt lietotājus.</li><li>Veicot reģistrāciju tiek nosūtīts epasts.</li></ul><p>Alpha 1.7.1</p><ul><li>Ieviesu kategorijas un apak&scaron;kategorijas redzamības noteik&scaron;anu. (Var noteikt kuras kategorijas redz reģistrēts lietotājs un kuras nereģistrēts)</li></ul>', 1443277680),
(132, 54, 1, 'Alpha 1.5', '<p>Pateicoties Martai, izlabotas dažas drukas kļūdas. (Arī &scaron;ajā aprakstā) Pilnībā pārveidota reģistrācija.</p>', 1437583399),
(133, 54, 1, 'Alpha 1.6.1', '<p>Izlaboti sīkumi sakarā ar attēlu aug&scaron;upielādi.</p><p>Ja raksta autors ir dzēsts, tas arī attiecīgi uzrādas.</p><p>Pievienota elementārs teksta redaktors, kas pagaidām arī apmierina.</p><p>Pievienota iespēja apskatīt pārejos lietotājus.</p>', 1439812116),
(136, 54, 1, 'Alpha 1.7', '<ul><li>Pievienoju saites uz nikiem.</li><li>Administratoram iespēju dzēst rakstu(kas dzē&scaron;&nbsp;visu rakstu, ieskaitot komentārus) un arī iespēju dzēst konkrētu komentāru.</li><li>Pārtaisīju lapu lai viņā var iekļūt bez reģistrācijas. Neregistrēts lietotajs var tikai lasīt rakstus un apskatīt lietotājus.</li><li>Nākamais ir pievienot kategorijas redzamības sistēmu (Vai viņa ir redzama visiem, vai arī tikai reģistrētiem lietotājiem).</li></ul>', 1441221063);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'user'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `sub_cat`
--

CREATE TABLE IF NOT EXISTS `sub_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `guest` int(1) NOT NULL,
  `user` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `sub_cat`
--

INSERT INTO `sub_cat` (`id`, `cat_id`, `title`, `text`, `guest`, `user`) VALUES
(1, 1, 'Jaunumi', 'Jaunumi par šo forumu', 1, 1),
(6, 4, 'Remonts', 'Viss par un ap remontu', 0, 1),
(7, 4, 'Detaļas', 'Meklē detaļu vai arī ir kāda lieka? Raksti te!', 0, 1),
(8, 5, 'Remonts', 'Prasam kā taisīt, skaidrojam ka taisīt, jeb viss par un ap remontu', 0, 1),
(9, 5, 'Detaļas', 'Meklē detaļu vai arī ir kāda lieka, raksti te!', 0, 1),
(10, 5, 'Modeļi', 'Apsriežam konkrētus modeļus, viedokļi, iespaidi un viss pārejais', 0, 1),
(11, 5, 'Tirgus', 'Tirgojam visu kas ir saistīts ar Ducati', 0, 1),
(12, 4, 'Modeļi', 'Diskutējam par jebkuru Hondas modeli', 0, 1),
(13, 4, 'Tirgus', 'Tirgojam visu saistībā ar Hondu', 0, 1),
(14, 6, 'Testa apakškategorija', 'Šis ir testa piemērs', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `invite_code` varchar(225) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=65 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `invite_code`) VALUES
(54, 'bubis7', '$2y$10$3gTATsSUvrOaLq.32AkkzulFV92mdg4tU2fDWiWt3s17pPw2v/YjS', ''),
(62, 'Klusais', '$2y$10$9.Aks.1t8KlSzJtqTxxcqey4BzpE4d0jPQdbQXRv/SAb.iURSSPtG', '3'),
(63, 'guntars', '$2y$10$QhSZFYoi3aRCA44qNSCT3el0jsj6rUR4PEStAWi65OwWTiTSWfZUm', '4'),
(64, 'martakanepite', '$2y$10$htsyRJeHjkS120KMAghpkOi4JxpX9osVmq8x6pw.cT53QoS8zJXy2', '6');

-- --------------------------------------------------------

--
-- Table structure for table `users_info`
--

CREATE TABLE IF NOT EXISTS `users_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `avatar` varchar(255) NOT NULL DEFAULT 'no-avatar.png',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `users_info`
--

INSERT INTO `users_info` (`id`, `users_id`, `firstname`, `lastname`, `email`, `gender`, `location`, `avatar`) VALUES
(43, 54, 'Bruno', 'Krūmiņš', 'brunokrumins@gmail.com', 'male', 'Mežciems', '1443100130_2013-12-20 16.45.48.jpg'),
(51, 62, 'Kristiāns', 'Timošenko', 'klusais52@gmail.com', 'male', 'Pļavnieki', 'no-avatar.png'),
(52, 63, '', '', 'guntars.ducati@gmail.com', '', '', 'no-avatar.png'),
(53, 64, '', '', 'martakanepite@inbox.lv', '', '', 'no-avatar.png');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `users_id`, `roles_id`) VALUES
(1, 54, 2),
(0, 62, 1),
(0, 63, 2),
(0, 64, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
