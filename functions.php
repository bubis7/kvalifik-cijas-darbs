<!-- Visu funkciju apkopojums -->
<?php 
require 'connect.php';
$root = '/Forum/';
function mysqli_result($res,$row=0,$col=0){ 
    $numrows = mysqli_num_rows($res); 
    if ($numrows && $row <= ($numrows-1) && $row >=0){
        mysqli_data_seek($res,$row);
        $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
        if (isset($resrow[$col])){
            return $resrow[$col];
        }
    }
    return false;
}

function user_info($username,$fieldname){
    global $connect;
    $query = "SELECT `$fieldname` FROM `users_info` WHERE `users_id` = '$username' ";
    $field = mysqli_query($connect, $query);
    while ($i = mysqli_fetch_array($field, MYSQLI_BOTH)){
        return $i[$fieldname];
    }
}
function getowner($id){
    global $connect;
    $q1 = mysqli_query($connect, "SELECT `users_id` FROM `posts` WHERE `id`='$id'");
    $qnr1 = mysqli_num_rows($q1);

    if($qnr1 == 1){
        $users_id = mysqli_result($q1 , 0 , 0);
        if($users_id==$_SESSION['user_id']){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }

}
function getownercom($id){
    global $connect;
    $q = mysqli_query($connect, "SELECT `user_id` FROM `comments` WHERE `id` = '$id'");
    $qnr1 = mysqli_num_rows($q);

    if($qnr1 == 1){
        $users_id = mysqli_result($q , 0 , 0);
        if($users_id==$_SESSION['user_id']){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}
function username_by_id($id){
    global $connect;
    $q1 = mysqli_query($connect, "SELECT `username` FROM `users` WHERE `id`='$id'");
    $username = mysqli_result($q1,0,'username');
    return $username;
}
function user_info_by_id($id,$fieldname){
    global $connect;
    $q1 = mysqli_query($connect, "SELECT `$fieldname` FROM `users_info` WHERE `users_id` = '$id'");
    $re = mysqli_result($q1,0,0);
    return $re;
}
function cat_name($id){
    global $connect;
    $q1 = mysqli_query($connect, "SELECT `title` FROM `sub_cat` WHERE `id` = '$id'");
    $re = mysqli_result($q1,0,0);
    return $re;
}
function post_name($id){
    global $connect;
    $q1 = mysqli_query($connect, "SELECT `title` FROM `posts` WHERE `id` = '$id'");
    $re = mysqli_result($q1,0,0);
    return $re;
}
function allowed_cat($id){
    global $connect;
    if(loggedin()){
        $q1 = mysqli_query($connect, "SELECT `user` FROM `sub_cat` WHERE `id` = '$id'");
        while($rq1 = mysqli_fetch_array($q1)){
            if($rq1['user']==1){
                $view = true;
            }else{
                $view = false;
            }
        }
    }elseif(!loggedin()){
        $q1 = mysqli_query($connect, "SELECT `guest` FROM `sub_cat` WHERE `id` = '$id'");
        while($rq1 = mysqli_fetch_array($q1)){
            if($rq1['guest']==1){
                $view = true;
            }else{
                $view = false;
            }
        }
    }else{
        $view = false;
    }
return $view;
}
function allowed_post($id){
    global $connect;
    $q1 = mysqli_query($connect, "SELECT `sub_cat_id` FROM `posts` WHERE `id` = '$id'");
    while($rq1 = mysqli_fetch_array($q1)){
        $sub_cat = $rq1['sub_cat_id'];
    }
    if(allowed_cat($sub_cat)){
        $view = true;
    }else{
        $view = false;
    }
    return $view;
}
?>