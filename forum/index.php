<!-- Foruma sākumlapa -->
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="../img/favicon.ico" />
	<link href="../css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dukatisti</title>
</head>
<body class="body">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<?php 
require "../core.php";
require "../connect.php";
require "../functions.php";
include "../navbar.php";
?>
<div class="post_view">
	<ol class="breadcrumb">
		<li><a href="<?php echo $root ?>forum/">Sākums</a></li>
	</ol>
</div>
<?php
if(loggedin()){
	$q1 = mysqli_query($connect, "SELECT * FROM `cat` WHERE `user`=1 ORDER BY id Asc");
}elseif(!loggedin()){
	$q1 = mysqli_query($connect, "SELECT * FROM `cat` WHERE `guest`=1 ORDER BY id Asc");
}
while($rq1 = mysqli_fetch_array($q1)){
	?>
	<div class="cat_view panel panel-default">
		<div class="panel-heading">
			
			<h4>
			<span class="glyphicon glyphicon-comment"></span>			
				<?php
				echo $rq1['title'];
				if(loggedin()){
					$q2 = mysqli_query($connect, "SELECT * FROM `sub_cat` WHERE `user`=1 AND `cat_id`='".$rq1['id']."'");
				}elseif(!loggedin()){
					$q2 = mysqli_query($connect, "SELECT * FROM `sub_cat` WHERE `guest`=1 AND `cat_id`='".$rq1['id']."'");
				}
				?>
				</h4>
		</div>
		<div class="panel-body">
			<ul class="nav nav-pills nav-stacked">
			<?php
			while($rq2 = mysqli_fetch_array($q2)){
				echo '<li><a href="sub_category.php?category='.$rq2['id'].'">'.$rq2['title'].'</a></li>';
				echo "<p>".$rq2['text']."</p>";
			}
			?>
			</ul>
		</div>
	</div>
<?php
}
include '../footer.php';
?>
	
</body>
</html>
