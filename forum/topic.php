<!-- Raksta skata lapa -->
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="../img/favicon.ico" />
	<link href="../css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dukatisti</title>
	<script src="//cdn.ckeditor.com/4.5.2/basic/ckeditor.js"></script>
</head>
<body class="body">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<?php 

date_default_timezone_set("Europe/Riga");
require("../connect.php");
require("../core.php");
require("../functions.php");
include "../navbar.php";

if(!allowed_post($_GET['topic'])){
	header("Location:../");
}

	if(isset($_GET['del_post'])){
		if(getadmin()==true){
			$q2 = mysqli_query($connect, "SELECT * FROM `posts` WHERE `id`='".$_GET['topic']."'") ;
			if(mysqli_num_rows($q2)==1){
				if(mysqli_query($connect, "DELETE FROM `posts` WHERE `id`='".$_GET['topic']."'")){
					if(mysqli_query($connect, "DELETE FROM `comments` WHERE `posts_id`='".$_GET['topic']."'")){
					echo 'Done!';
					Header("Location:".$root."forum");
					}
				}else echo mysqli_error();
			}else echo 'Kluda';
		}	
	}
	if(isset($_GET['dc'])){
		if(getadmin()==true){
			$q2 = mysqli_query($connect, "SELECT * FROM `comments` WHERE `id`='".$_GET['dc']."'");
			if(mysqli_num_rows($q2)==1){
				if(mysqli_query($connect, "DELETE FROM `comments` WHERE `id`='".$_GET['dc']."'")){
					echo 'Done!';
					Header("Location:".$root."forum/topic.php?topic=".$_GET['topic']."");
				}
			}
		}
	}

$q1 = mysqli_query($connect, "SELECT * FROM `posts` WHERE `id`='".$_GET['topic']."'");
$rq1 = mysqli_fetch_array($q1);


$q3 = mysqli_query($connect, "SELECT * FROM `comments` where `posts_id`='".$rq1['id']."'");

if(isset($_POST['edit_post'])){
	if(!empty($_POST['com_text'])){
		$text = mysqli_real_escape_string($connect, $_POST['com_text']);
		$edit_time = time();
		if(mysqli_query($connect, "UPDATE `posts` SET `text` = '$text' WHERE `id`='".$_GET['topic']."'")){
			header("Location:topic.php?topic=".$_GET['topic']."");
		}
	}
}
if(isset($_POST['edit_com'])){
	if(!empty($_POST['edit_com'])){
		$text = mysqli_real_escape_string($connect, $_POST['edit_com_text']);
		$edit_time = time();
		if(mysqli_query($connect, "UPDATE `comments` SET `text` = '$text' WHERE `id` = '".$_POST['com_id']."'")){
			header("Location:topic.php?topic=".$_GET['topic']."");
		}
	}
}

if(isset($_POST['comment'])){
	if(!empty($_POST['com_text'])){
		$text = mysqli_real_escape_string($connect, $_POST['com_text']);
		$time = time();
		if (mysqli_query($connect, "INSERT INTO `comments` VALUES ('','".$rq1['id']."','".$_SESSION['user_id']."','$text','$time')")) {
			//echo "comentars pievienots";
			header("Location:topic.php?topic=".$_GET['topic']."");
		}else{
			//echo "kluda pievienojot komentaru";
		}
	}

}
?>
<div class="post_view">
	<ol class="breadcrumb">
		<li><a href="<?php echo $root ?>forum/">Sākums</a></li>
		<li><a href="<?php echo $root ?>forum/sub_category.php?category=<?php echo $rq1['sub_cat_id'] ?>"><?php echo cat_name($rq1['sub_cat_id']);?></a></li>
		<li class="active"><a href="<?php echo $root ?>forum/topic.php?topic=<?php echo $rq1['id'] ?>"><?php echo post_name($rq1['id']);?></a></li>
	</ol>
</div>
<div class="post_view">
	

	<div class="row">			
		<div class="col-md-2 user">
			<?php echo '<img src="../img/Avatars/'.user_info($rq1['users_id'],'avatar').'" width="100px" alt="">';?>
			<p><a href="../user/?user=<?php echo $rq1['users_id'];?>"><?php echo username_by_id($rq1['users_id']) ?></a></p>
			<p><?php echo user_info($rq1['users_id'],'firstname'); 
					 echo ' ', user_info($rq1['users_id'],'lastname'); ?></p>
		</div>
		<div class="col-md-6 post">
			<?php
				if(loggedin() && getadmin()==true){
					?>
					<a class="btn btn-danger btn-xs" style="position:relative; left:10px;" onclick="return confirm(\'Vai tiesam gribi dzest?\')" href="?topic=<?php echo $_GET['topic'];?>&del_post=true">Dzēst</a>
					<a class="btn btn-warning btn-xs" style="position:relative; left:15px;" href="?topic=<?php echo $_GET['topic'];?>&edit_post=true">Labot</a>
					<?php	
				}elseif(loggedin() && getowner($_GET['topic'])){
					?>
					<a class="btn btn-warning btn-xs" style="position:relative; left:15px;" href="?topic=<?php echo $_GET['topic'];?>&edit_post=true">Labot</a>
					<?php
				}
			?>
			<h1><?php echo htmlentities($rq1['title']) ?></h1>
			<p><?php echo $rq1['text'];?></p>
			<p><?php echo date("H:i:s d.m.Y", $rq1['time']) ?></p>
		</div>
	</div>
</div>
<?php
if(isset($_GET['edit_post'])){
	if(getadmin()==true || getowner($_GET['topic'])){
		$q2 = mysqli_query($connect, "SELECT * FROM `posts` WHERE `id`='".$_GET['topic']."'");
		while($rq2 = mysqli_fetch_array($q2)){
			?>
			<center>
			<form action="" method="POST" style="margin: 0px 10% 0px 10%;">
			<table>
			<tr><td><textarea type="text" name="com_text"  rows="10" cols="70" ><?php echo $rq2['text'];?></textarea></td></tr>
			<script>CKEDITOR.replace('com_text', {

				});</script>
			<tr><td><input type="submit" name="edit_post"></td></tr>
			</table>
			</form>
			</center>
			<?php
		}
	}
}

while($rq3 = mysqli_fetch_array($q3)){
	?>
	<div class="post_view">
		<div class="row">
			<div class="col-md-2 user">
				<?php echo '<img src="../img/Avatars/'.user_info($rq3['user_id'],'avatar').'" width="100px" alt="">';?>
				<p><a href="../user/?user=<?php echo $rq3['user_id'];?>"><?php echo username_by_id($rq3['user_id']) ?></a></p>
				<p><?php echo user_info($rq3['user_id'],'firstname'); 
					 echo ' ', user_info($rq3['user_id'],'lastname'); ?></p>
			</div>
			<div class="col-md-6 post">
				<?php
				if(loggedin() && getadmin()==true){
					?>
					<a class="btn btn-danger btn-xs" style="position:relative; left:10px;" onclick="return confirm(\'Vai tiesam gribi dzest?\')" href="?topic=<?php echo $_GET['topic'];?>&dc=<?php echo $rq3['id'];?>">Dzēst</a>
					<a class="btn btn-warning btn-xs" style="position:relative; left:15px;" href="?topic=<?php echo $_GET['topic'];?>&edit_comment=<?php echo $rq3['id'];?>">Labot</a>
					<?php	
				}elseif(loggedin() && getownercom($rq3['id'])){
					?>
					<a class="btn btn-warning btn-xs" style="position:relative; left:15px;" href="?topic=<?php echo $_GET['topic'];?>&edit_comment=<?php echo $rq3['id'];?>">Labot</a>
					<?php
				}
				?>
				<p><?php echo ($rq3['text']) ?></p>
				<p><?php echo date("H:i:s d.m.Y", $rq3['time']) ?></p>
			</div>
		</div>
	</div>
	<?php

if(isset($_GET['edit_comment'])){
	if(getadmin()==true || getownercom($rq3['id'])){
		if($rq3['id']==$_GET['edit_comment']){
			$q2 = mysqli_query($connect, "SELECT * FROM `comments` WHERE `id`='".$_GET['edit_comment']."'");
			while($rq2 = mysqli_fetch_array($q2)){
				?>
				<center>
				<form action="" method="POST" style="margin: 0px 10% 0px 10%;">
				<input type="hidden" name="com_id" value="<?php echo $_GET['edit_comment'];?>">
				<table>
				<tr><td><textarea type="text" name="edit_com_text"  rows="10" cols="70" ><?php echo $rq2['text'];?></textarea></td></tr>
				<script>CKEDITOR.replace('edit_com_text', {

					});</script>
				<tr><td><input type="submit" name="edit_com"></td></tr>
				</table>
				</form>
				</center>
				<?php
			}
		}
	}
}
}


if(loggedin()){
?>
<center>
<form action="" method="POST" style="margin: 0px 10% 0px 10%;">
	<table>
		<tr><td>Pievienot komentaru.</td></tr>
		<tr><td><textarea type="text" name="com_text" rows="10" cols="70" ></textarea></td></tr>
		<script>CKEDITOR.replace('com_text', {

			});</script>
		<tr><td><input type="submit" name="comment"></td></tr>
	</table>
</form>
</center>
<?php
}
include '../footer.php';
?>

</body>
</html>
