<!-- Apakškategorijas skata lapa -->
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="../img/favicon.ico" />
	<link href="../css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dukatisti</title>
</head>
<body class="body">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<?php
require("../connect.php");
require("../core.php");
require("../functions.php");
include "../navbar.php";

if(!allowed_cat($_GET['category'])){	
	header("Location:../");
}

if(loggedin()){
	$q1 = mysqli_query($connect, "SELECT * FROM `sub_cat` WHERE `id` = '".$_GET['category']."'");
	while($rq1 = mysqli_fetch_array($q1)){
		if($rq1['user']==1){
			$view = true;
		}else{
			$view = false;
		}
	}
}elseif(!loggedin()){
	$q1 = mysqli_query($connect, "SELECT * FROM `sub_cat` WHERE `id` = '".$_GET['category']."'");
	while($rq1 = mysqli_fetch_array($q1)){
		if($rq1['guest']==1){
			$view = true;
		}else{
			$view = false;
		}
	}
}else{
	$view = false;
}



if($_GET['category']){
$q1 = mysqli_query($connect, "SELECT * FROM `posts` WHERE `sub_cat_id`='".$_GET['category']."' ORDER BY id DESC");
		?>
		
		<div class="post_view">
			<ol class="breadcrumb">
				<li><a href="<?php echo $root ?>forum/">Sākums</a></li>
				<li class="active"><a href="<?php echo $root ?>forum/sub_category.php?category=<?php echo $_GET['category'] ?>"><?php echo cat_name($_GET['category']);?></a></li>
			</ol>
		</div>
		<?php
		if(loggedin()){
			?>
			<center>
				<a class="btn btn-default" style="margin-bottom:10px;" href="newpost.php?category=<?php echo $_GET['category'] ?>">Izveidot rakstu!</a>
			</center>
			<?php
		}
		?>	
		<div class="sub_cat_view">
		
		<?php
		while($rq1 = mysqli_fetch_array($q1)){
			?>
			<div class="panel panel-default">
				<div class="panel-heading" style="padding:0px;">
					<ul class="nav nav-pills nav-stacked">
						<?php echo '<li><a href="topic.php?topic='.$rq1['id'].'">'.$rq1['title'].'</a></li>'; ?>
					</ul>
				</div>
				<div class="panel-body body2">
					<?php 
					if(!empty(username_by_id($rq1['users_id']))){
						?>
							<p>Rakstu izveidoja <a href="../user/?user=<?php echo $rq1['users_id'];?>"><?php echo username_by_id($rq1['users_id']);?></a>, <?php echo date("d.m.Y", $rq1['time']);?></p>
						<?php
					}else{
						?>
						<p>Rakstu izveidoja dzēsts lietotājs, <?php echo date("d.m.Y", $rq1['time']);?></p>
						<?php
					}
					?>
				</div>
			</div>
			<?php			
		}
		?>
		</div>
		<?php

}
include '../footer.php';
?>	
</body>
</html>


