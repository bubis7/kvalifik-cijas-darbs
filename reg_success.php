<!-- Veiksmīgas reģistrācijas paziņojuma lapa -->
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" href="img/favicon.ico" />
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dukatisti</title>
</head>
<body class="body">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<div>
	<center>
		<img src="img/Logo.png" alt="">
		<div class="post_view">
			<p>Reģistrācija notikusi veiksmīgi!</p>
			<p>Jūs automātiski atgriezīsieties autorizācijas lapā.</p>
		</div>
	</center>	
</div>
<?php 
include "functions.php";
header("Refresh:5;URL=".$root."");

?>
</body>
</html>